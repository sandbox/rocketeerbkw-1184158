<?php

function exponent_preprocess_search_block_form(&$vars, $hook) {
  // Set a default value for the search box
  $vars['form']['search_block_form']['#value'] = t('search...');

  // Add a custom class to the search box
  $curClass = $vars['form']['search_block_form']['#attributes']['class'];
  $vars['form']['search_block_form']['#attributes'] = array('class' => $curClass . ' toggleval');

  // Rebuild the rendered version (search form only, rest remains unchanged)
  unset($vars['form']['search_block_form']['#printed']);
  $vars['search']['search_block_form'] = drupal_render($vars['form']['search_block_form']);

  // Collect all form elements to make it easier to print the whole form.
  $vars['search_form'] = implode($vars['search']);
}
